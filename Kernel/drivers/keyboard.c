#include <keyboard.h>

#define BUFFER_SIZE 512
#define TRUE 1
#define FALSE 0
typedef int bool;

uint8_t read_port(uint8_t* port);


uint8_t keyboard[2][0x3A] =
{
    {    /*  0x?0  0x?1  0x?2  0x?3  0x?4  0x?5  0x?6  0x?7  0x?8  0x?9  0x?A  0x?B  0x?C  0x?D  0x?E  0x?F  */
    /*0x0?*/ 0x00, 0x00, '1',  '2',  '3',  '4',  '5',  '6',  '7',  '8',  '9',  '0',  '-',  '=',  '\b', '\t',
    /*0x1?*/ 'q',  'w',  'e',  'r',  't',  'y',  'u',  'i',  'o',  'p',  '[',  ']',  '\n', 0x00, 'a',  's',
    /*0x2?*/ 'd',  'f',  'g',  'h',  'j',  'k',  'l',  ';',  '\'', '`',  0x2A, '\\', 'z',  'x',  'c',  'v',
    /*0x3?*/ 'b',  'n',  'm',  ',',  '.',  '/',  0x36, '*',  0x00, ' ' },
    {    /*  0x?0  0x?1  0x?2  0x?3  0x?4  0x?5  0x?6  0x?7  0x?8  0x?9  0x?A  0x?B  0x?C  0x?D  0x?E  0x?F  */
    /*0x0?*/ 0x00, 0x00, '!',  '@',  '#',  '$',  '%',  '^',  '&',  '*',  '(',  ')',  '_',  '+',  '\b', '\t',
    /*0x1?*/ 'Q',  'W',  'E',  'R',  'T',  'Y',  'U',  'I',  'O',  'P',  '{',  '}',  '\n', 0x00, 'A',  'S',
    /*0x2?*/ 'D',  'F',  'G',  'H',  'J',  'K',  'L',  ':',  '"',  '~',  0xAA, '|',  'Z',  'X',  'C',  'V',
    /*0x3?*/ 'B',  'N',  'M',  '<',  '>',  '?',  0xB6, '*',  0x00, ' ' }
};

uint8_t buffer[BUFFER_SIZE];

unsigned int w = 0;
unsigned int r = 0;
bool left_shift = FALSE;
bool right_shift = FALSE;

void keyboard_handler()
{
    uint8_t keycode, type = left_shift || right_shift ? 1 : 0;
    uint8_t c;

    if(read_port(0x64) & 0x01)
    {
        keycode = read_port((uint8_t*)0x60);
        switch(keycode)
        {
            case 0x2A:
                left_shift = TRUE;
                break;

            case 0x2A | 0x80:
                left_shift = FALSE;
                break;

            case 0x36:
                right_shift = TRUE;
                break;

            case 0x36 | 0x80:
                right_shift = FALSE;
                break;

            default:
                if(keycode >= 0x00 && keycode < 0x3A && keyboard[type][keycode] != 0x00)
                {
                    c = keyboard[type][keycode];
                    if(c && ((c < '\b') || ((c > '\n') && (c < ' ')) || (c > '~') ? 0 : 1))
                    {
                        buffer[w] = c;
                        w = (w + 1) % BUFFER_SIZE;
                    }
                }
                break;
        }
    }
}

uint8_t getchar()
{
    if(r == w)
    {
        return 0;
    }
    uint8_t ret = buffer[r];
    r = (r + 1) % BUFFER_SIZE;
    return ret;
}
