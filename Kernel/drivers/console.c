// CONSOLE DRIVER ADAPTED FROM NAIVE CONSOLE PROVIDED WITH X86BAREBONES

#include <console.h>

#define HEIGHT (25)
#define WIDTH (80)
#define START ((uint8_t*)0xB8000)
#define SIZE (HEIGHT * WIDTH * 2)
#define END ((uint8_t*)(START + SIZE))
#define BOTTOM ((uint8_t*)(START + (HEIGHT - 1) * WIDTH * 2))

static char buffer[64] = { '0', 0 };
static uint8_t* current = BOTTOM;

static uint32_t uint2base(uint64_t value, char* buffer, uint32_t base)
{
    char *p = buffer;
    char *p1, *p2;
    uint32_t digits = 0;

    //Calculate characters for each digit
    do
    {
        uint32_t remainder = value % base;
        *p++ = (remainder < 10) ? remainder + '0' : remainder + 'A' - 10;
        ++digits;
    }
    while(value /= base);

    // Terminate string in buffer.
    *p = 0;

    //Reverse string in buffer.
    p1 = buffer;
    p2 = p - 1;
    while(p1 < p2)
    {
        char tmp = *p1;
        *p1 = *p2;
        *p2 = tmp;
        ++p1;
        --p2;
    }

    return digits;
}

void console_print_base(uint64_t value, uint32_t base)
{
    uint2base(value, buffer, base);
    console_print(buffer);
}

void console_print(const char* string)
{
    for(int i = 0; string[i] != 0; i++)
    {
        console_print_char(string[i]);
    }
}

void console_println(const char* string)
{
    console_print(string);
    console_newline();
}

void console_print_char(char character)
{
    switch(character)
    {
        case '\b':
            console_backspace();
            return;

        case '\n':
            console_newline();
            return;

        case '\t':
            console_print_char(' ');
            return;

        default:
            if((uint64_t)(current + 2 - START) % (WIDTH * 2) != 0)
            {
                *current = character;
                current += 2;
            }
            return;
    }
}

void console_newline(void)
{
    current = START;
    for(uint8_t* source = (START + WIDTH * 2); source < END; source += 2)
    {
        *current = *source;
        current += 2;
    }

    while(current < END)
    {
        *current = ' ';
        current += 2;
    }
    current = BOTTOM;
}

void console_backspace(void)
{
    if((uint64_t)(current - START) % (WIDTH * 2) != 0)
    {
        current -= 2;
        *current = ' ';
    }
}

void console_print_dec(uint64_t value)
{
    console_print_base(value, 10);
}

void console_print_hex(uint64_t value)
{
    console_print_base(value, 16);
}

void console_print_bin(uint64_t value)
{
    console_print_base(value, 2);
}

void console_clear(void)
{
    for(current = START; current < END; current += 2)
    {
        *current = ' ';
    }
    current = BOTTOM;
}

void console_print_time(int hours, int minutes, int seconds)
{
    if(hours < 10)
    {
        console_print_char('0');
    }
    console_print_dec(hours);
    console_print_char(':');
    if(minutes < 10)
    {
        console_print_char('0');
    }
    console_print_dec(minutes);
    console_print_char(':');
    if(seconds < 10)
    {
        console_print_char('0');
    }
    console_print_dec(seconds);
}
