#ifndef SYSCALL_H
#define SYSCALL_H

#include <stdint.h>

void syscall_dispatcher(uint64_t rax, uint64_t rbx, uint64_t rcx, uint64_t rdx);

#endif
