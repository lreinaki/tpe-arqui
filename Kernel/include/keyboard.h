#ifndef KEYBOARD_H
#define KEYBOARD_H
#include <stdint.h>

void keyboard_handler(void);
uint8_t getchar(void);

#endif
