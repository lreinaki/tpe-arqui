#ifndef CONSOLE_H
#define CONSOLE_H

#include <stdint.h>

void console_print(const char* string);
void console_println(const char* string);
void console_print_char(char character);
void console_newline(void);
void console_backspace(void);
void console_print_dec(uint64_t value);
void console_print_hex(uint64_t value);
void console_print_bin(uint64_t value);
void console_print_time(int hours, int minutes, int seconds);
void console_clear(void);

#endif
