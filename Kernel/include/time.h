#ifndef TIME_H
#define TIME_H

void get_time(int* hours, int* minutes, int* seconds);
void set_time(int hours, int minutes, int seconds);
void time_handler(void);

#endif
