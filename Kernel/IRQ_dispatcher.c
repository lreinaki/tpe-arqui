#include <time.h>
#include <keyboard.h>
#include <stdint.h>

void keyboard_interrupt(void);  // IMPLEMENTED IN ASM/INTERRUPTS.ASM
void timer_interrupt(void);  // IMPLEMENTED IN ASM/INTERRUPTS.ASM

void IRQ_dispatcher(uint64_t irq)
{
    switch(irq)
    {
        case 1:
            time_handler();  // INT 21
            return;
        case 0:
            keyboard_handler();  // INT 20
            return;
    }
}
