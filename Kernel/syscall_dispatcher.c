#include <syscall_dispatcher.h>
#include <time.h>
#include <console.h>

void syscall_dispatcher(uint64_t syscall, uint64_t a, uint64_t b, uint64_t c)
{
    switch(syscall)
    {
        case 0:  // WRITE
            console_print_char(a);
            break;
        //case 1:  // CLEAR
            //console_clear();
            //break;
        case 2:  // READ
            *((uint8_t*)a) = getchar();
            break;
        case 3:  // TIME
            get_time((int*)a, (int*)b, (int*)c);
            break;
        case 4:  // STIME
            set_time(a, b, c);
            break;
    }
}
