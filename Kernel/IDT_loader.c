#include <stdint.h>
#include <IDT_loader.h>

#pragma pack(push)
#pragma pack (1)
struct { uint16_t offset_l; uint16_t segment_selector; uint8_t zero_1; uint8_t access; uint16_t offset_m; uint32_t offset_h; uint32_t zero_2; }* idt = 0;
#pragma pack(pop)

void keyboard_interrupt(void);  // IMPLEMENTED IN ASM/INTERRUPTS.ASM
void timer_interrupt(void);  // IMPLEMENTED IN ASM/INTERRUPTS.ASM
void zero_division_handler(void);  // IMPLEMENTED IN ASM/INTERRUPTS.ASM
void overflow_handler(void);  // IMPLEMENTED IN ASM/INTERRUPTS.ASM
void invalid_opcode_handler(void);  // IMPLEMENTED IN ASM/INTERRUPTS.ASM
void system_call_handler(void);  // IMPLEMENTED IN ASM/INTERRUPTS.ASM
void pic_master_mask(uint8_t mask);  // IMPLEMENTED IN ASM/INTERRUPTS.ASM
void pic_slave_mask(uint8_t mask);  // IMPLEMENTED IN ASM/INTERRUPTS.ASM
void enable_interrupts(void);  // IMPLEMENTED IN ASM/INTERRUPTS.ASM

static void add_entry(int index, uint64_t offset)
{
    idt[index].segment_selector = 0x08;
    idt[index].offset_l = offset & 0xFFFF;
    idt[index].offset_m = (offset >> 16) & 0xFFFF;
    idt[index].offset_h = (offset >> 32) & 0xFFFFFFFF;
    idt[index].access = 0x8E;
    idt[index].zero_1 = 0;
    idt[index].zero_2 = 0;
}

void load_idt()
{
    // Zero Division Exception
    add_entry(0x00, (uint64_t)&zero_division_handler);
    // Overflow Exception
    add_entry(0x04, (uint64_t)&overflow_handler);
    // Invalid OpCode Exception
    add_entry(0x06, (uint64_t)&invalid_opcode_handler);
    // Timer tick
    add_entry(0x21, (uint64_t)&timer_interrupt);
    // Keyboard
    add_entry(0x20, (uint64_t)&keyboard_interrupt);
    // System Call
    add_entry(0x80, (uint64_t)&system_call_handler);


    pic_master_mask(0xFC);
    pic_slave_mask(0xFF);

    enable_interrupts();
}
