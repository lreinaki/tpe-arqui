#include <stdint.h>
#include <lib.h>
#include <module_loader.h>
#include <console.h>

static uint32_t read_uint32(uint8_t** address)
{
    uint32_t result = *(uint32_t*)(*address);
    *address += sizeof(uint32_t);
    return result;
}

static void load_module(uint8_t** module, void* target_module_address)
{
    uint32_t module_size = read_uint32(module);

    console_print("  Will copy module at 0x");
    console_print_hex((uint64_t)*module);
    console_print(" to 0x");
    console_print_hex((uint64_t)target_module_address);
    console_print(" (");
    console_print_dec(module_size);
    console_print(" bytes)");

    memcpy(target_module_address, *module, module_size);
    *module += module_size;

    console_println(" [ Done ]");
}

void load_modules(void* payload_start, void** target_module_address)
{
    uint8_t* currentModule = (uint8_t*)payload_start;
    uint32_t moduleCount = read_uint32(&currentModule);

    for (int i = 0; i < moduleCount; ++i)
    {
        load_module(&currentModule, target_module_address[i]);
    }
}
