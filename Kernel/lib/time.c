#include <time.h>

void write_hours(int hours);  // IMPLEMENTED IN ASM/TIME.ASM
void write_minutes(int minutes);  // IMPLEMENTED IN ASM/TIME.ASM
void write_seconds(int seconds);  // IMPLEMENTED IN ASM/TIME.ASM

int get_hours();  // IMPLEMENTED IN ASM/TIME.ASM
int get_seconds();  // IMPLEMENTED IN ASM/TIME.ASM
int get_minutes();  // IMPLEMENTED IN ASM/TIME.ASM

void get_time(int* hours, int* minutes, int* seconds)
{
    *hours = get_hours();
    *minutes = get_minutes();
    *seconds = get_seconds();
}

void set_time(int hours, int minutes, int seconds)
{
    if(hours < 0 || hours >= 24 || minutes < 0 || minutes >= 60 || seconds < 0 || seconds >= 60)
    {
        return;
    }
    write_seconds(seconds);
    write_minutes(minutes);
    write_hours(hours);
}

void time_handler(void)
{
    int seconds = get_seconds();
    if(seconds >= 60)
    {
        write_seconds(seconds - 60);
        write_minutes(get_minutes() + 1);
    }
    int minutes = get_minutes();
    if(minutes >= 60)
    {
        write_minutes(minutes - 60);
        write_hours(get_hours() + 1);
    }
    int hours = get_hours();
    if(hours >= 24)
    {
        write_hours(hours - 24);
    }
}
