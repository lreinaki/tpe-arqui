global loader

extern main  ; IMPLEMENTED IN KERNEL.C
extern initialize_kernel_binary  ; IMPLEMENTED IN KERNEL.C

loader:
    call initialize_kernel_binary  ; Set up the kernel binary, and get thet stack address
    mov rsp, rax  ; Set up the stack with the returned address
    call main

hang:
    cli
    hlt  ; halt machine should kernel return
    jmp hang
