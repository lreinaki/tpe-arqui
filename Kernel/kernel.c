#include <stdint.h>
#include <lib.h>
#include <module_loader.h>
#include <console.h>
#include <time.h>
#include <IDT_loader.h>

extern uint8_t text;  // DECLARED IN KERNEL.LD
extern uint8_t rodata;  // DECLARED IN KERNEL.LD
extern uint8_t data;  // DECLARED IN KERNEL.LD
extern uint8_t bss;  // DECLARED IN KERNEL.LD
extern uint8_t end_of_kernel_binary;  // DECLARED IN KERNEL.LD
extern uint8_t end_of_kernel;  // DECLARED IN KERNEL.LD

static const uint64_t page_size = 0x1000;

void* entry_point = (void*)0x400000;  // = userspace

void clear_bss(void* bbs_address, uint64_t bss_size)
{
    memset(bbs_address, 0, bss_size);
}

void* get_stack_base()
{
    return (void*)(
        (uint64_t)&end_of_kernel
        + page_size * 8  // The size of the stack itself, 32KiB
        - sizeof(uint64_t)  // Begin at the top of the stack
    );
}

void* initialize_kernel_binary(void)
{
    char buffer[128];

    console_println("[ MordorOS (x64BareBones) ]");
    console_println("[ Initializing kernel's binary ]");

    console_print("[ CPU Vendor: ");
    console_print(cpu_vendor(buffer));
    console_println(" ]");

    console_println("[ Loading modules ]");
    void* module_addresses[] = { entry_point };

    load_modules(&end_of_kernel_binary, module_addresses);

    clear_bss(&bss, &end_of_kernel - &bss);

    console_println("[ Loading IDT ]");
    load_idt();

    console_println("[ Done ]");
    return get_stack_base();
}

int main(void)
{
    console_print("\n\nWelcome to\n");
    console_print(" _____           _            _____ _____ \n");
    console_print("|     |___ ___ _| |___ ___   |     |   __|\n");
    console_print("| | | | . |  _| . | . |  _|  |  |  |__   |\n");
    console_print("|_|_|_|___|_| |___|___|_|    |_____|_____|\n");
    console_print("\n");
    console_print("Creators:\n  REINA KIPERMAN, Leandro Emmanuel\n  CRUZ, Maria Eugenia\n");
    console_print("\n\n");
    console_print("Type \"help\" and press ENTER to get a list of available commands\n");
    int exit_code = ((int (*)())entry_point)();
    console_print("[ Finished with exit code ");
    console_print_dec(exit_code);
    console_println(" ]");
    return 0;
}
