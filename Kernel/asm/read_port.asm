global read_port

read_port:
    mov rax,0
    push rdx
    mov rdx,rdi
    in al,dx
    pop rdx
    ret
