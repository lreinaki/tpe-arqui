global get_seconds
global get_minutes
global get_hours

global write_seconds
global write_minutes
global write_hours

get_seconds:
    mov rax,00h
    out 70h,al
    in al,71h
    ret

get_minutes:
    mov rax,02h
    out 70h,al
    in al,71h
    ret

get_hours:
    mov rax,04h
    out 70h,al
    in al,71h
    ret

write_seconds:
    push rax
    mov al,00h
    out 70h,al
    mov al,[rsp]
    out 71h,al
    pop rax
    ret

write_minutes:
    push rax
    mov al,02h
    out 70h,al
    mov al,[rsp]
    out 71h,al
    pop rax
    ret

write_hours:
    push rax
    mov al,04h
    out 70h,al
    mov al,[rsp]
    out 71h,al
    pop rax
    ret
