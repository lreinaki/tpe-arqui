GLOBAL cpu_vendor

section .text

cpu_vendor:
    mov rax, 0
    cpuid
    mov [rdi], ebx
    mov [rdi + 4], edx
    mov [rdi + 8], ecx
    mov byte [rdi + 13], 0
    mov rax, rdi
    ret
