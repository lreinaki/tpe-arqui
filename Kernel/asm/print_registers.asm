EXTERN console_print_hex  ; IMPLEMENTED IN DRIVERS/CONSOLE.C
EXTERN console_print  ; IMPLEMENTED IN DRIVERS/CONSOLE.C
EXTERN console_newline  ; IMPLEMENTED IN DRIVERS/CONSOLE.C

GLOBAL print_registers

%macro print_register 1
    mov rdi,s%1
    call console_print
    mov rdi,%1
    call console_print_hex
    call console_newline
%endmacro

SECTION .text

print_registers:
    push rdi
    print_register rax
    print_register rbx
    print_register rcx
    print_register rdx
    print_register rsi
    print_register rdi
    print_register rbp
    print_register rsp
    print_register r8
    print_register r9
    print_register r10
    print_register r11
    print_register r12
    print_register r13
    print_register r14
    print_register r15
    pop rdi
    ret

SECTION .data
    srax db "RAX: 0x", 0
    srbx db "RBX: 0x", 0
    srcx db "RCX: 0x", 0
    srdx db "RDX: 0x", 0
    srsi db "RSI: 0x", 0
    srdi db "RDI: 0x", 0
    srbp db "RBP: 0x", 0
    srsp db "RSP: 0x", 0
    sr8  db "R8:  0x", 0
    sr9  db "R9:  0x", 0
    sr10 db "R10: 0x", 0
    sr11 db "R11: 0x", 0
    sr12 db "R12: 0x", 0
    sr13 db "R13: 0x", 0
    sr14 db "R14: 0x", 0
    sr15 db "R15: 0x", 0
