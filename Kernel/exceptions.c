#include <console.h>

void exception_dispatcher(int exception)
{
    if(exception == 0)
    {
        console_println("Error: Division by zero");
    }
    if(exception == 1)
    {
        console_println("Error: Overflow");
    }
    if(exception == 2)
    {
        console_println("Error: Invalid OpCode");
    }
}
