#!/bin/bash
echo "Cleaning project"
make clean &> /dev/null
if [ $? = 0 ]
then
  echo "Cleaning successful"
  echo "Building project"
  make all &> /dev/null
  if [ $? = 0 ]
  then
    echo "Building successful"
    echo "Running"
    qemu-system-x86_64 -hda Image/x64BareBonesImage.qcow2 -m 512
  else
    echo "Building failed"
  fi
else
  echo "Cleaning failed"
fi
