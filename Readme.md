# Mordor OS is a basic implementation of [x86BareBones](https://bitbucket.org/RowDaBoat/x64barebones/overview) kernel and userspace.

# Installation

1. Install the following

        $ sudo apt install nasm qemu gcc make

2. From the project's root directory, run the kernel (this will automatically clean any previous build and rebuild from scratch)

        $ ./run.sh
