#ifndef SYSCALLS_H
#define SYSCALLS_H
#include <stdint.h>

void write(char c);
//void clear(void);
void read(uint8_t* c);
void get_time(int* hours, int* minutes, int* seconds);
void set_time(int hours, int minutes, int seconds);

#endif 
