#ifndef STRING_H
#define STRING_H

typedef char* string;

int strcmp(string a, string b);
string strcpy(string dest, string src);
string strfind(string str, char c);
string strip(string dest, string src);
int isprint(char c);

#endif
