#ifndef STDLIB_H
#define STDLIB_H

#include <stdarg.h>
#include <string.h>
#include <stdint.h>

void printf(const string string, ...);
void getchar(uint8_t* c);
void putchar(unsigned char c);

#endif
