#ifndef SHELL_H
#define SHELL_H

#include <stdint.h>
#include <string.h>

void shell_print(const string string);
void shell_println(const string string);
void shell_print_char(char character);
void shell_newline(void);
void shell_backspace(void);
void shell_print_dec(uint64_t value);
void shell_print_hex(uint64_t value);
void shell_print_bin(uint64_t value);
void shell_print_time(int hours, int minutes, int seconds);
void shell_clear(void);

#endif
