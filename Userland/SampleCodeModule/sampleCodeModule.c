#include <commands.h>
#include <syscalls.h>
#include <stdlib.h>
#include <shell.h>
#include <string.h>

int loop(void);

int main()
{
    //command_run("help");
    //command_run("quad -1 +0 +5");
    //command_run("lineal -1 +2");
    int exit = 0, i = 0;
    uint8_t c, buffer[81] = { 0 };
    while(!exit)
    {
        for(getchar(&c); c != '\n'; getchar(&c))
        {
            if(c && isprint(c))
            {
                printf("%c", c);
                if(c == '\b')
                {
                    if(i > 0)
                    {
                        buffer[i--] = 0;
                    }
                }
                else if(i < 80)
                {
                    buffer[i++] = c;
                    buffer[i] = 0;
                }
            }
        }
        printf("\n");
        command_run(buffer);
        buffer[0] = 0;
        i = 0;
    }
    return 0;
}
