#include <string.h>

int strcmp(string a, string b)
{
    int i, length;
    for(i = 0, length = 0; a[i] != 0; ++i, ++length);
    for(i = 0; b[i] != 0; ++i, --length);
    if(length == 0)
    {
        for(i = 0; a[i] != 0; ++i)
        {
            if(a[i] != b[i])
            {
                return 0;
            }
        }
        return 1;
    }
    return 0;
}

string strcpy(string dest, string src)
{
    for(int i = 0; src[i] != 0; ++i)
    {
        dest[i] = src[i];
    }
    return dest;
}

string strfind(string str, char c)
{
    for(int i = 0; str[i] != 0; ++i)
    {
        if(str[i] == c)
        {
            return str + i;
        }
    }
    return 0;
}

string strip(string dest, string src)
{
    int start, end;
    for(start = 0; src[start] != 0 && src[start] == ' '; ++start);
    for(end = 0; src[end] != 0; ++end);
    for(end -= 1; end >= 0 && src[end] == ' '; --end);
    for(int i = start; i <= end; ++i)
    {
        dest[i - start] = src[i];
    }
    dest[end - start + 1] = 0;
    return dest;
}

int isprint(char c)
{
    return (c < '\b') || ((c > '\n') && (c < ' ')) || (c > '~') ? 0 : 1;
}
