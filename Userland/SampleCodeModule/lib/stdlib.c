#include <stdlib.h>
#include <syscalls.h>
#include <shell.h>

void putchar(unsigned char c)
{
    shell_print_char(c);
}

void getchar(uint8_t* c)
{
    read(c);
}

void printf(const string str, ...)
{
    va_list args;
    va_start(args, str);

    uint16_t i = 0;
    while(str[i] != 0)
    {
        ++i;
    }
    uint16_t length = i;
    for(i = 0; i < length; ++i)
    {
        if(str[i] == '%')
        {
            if(i < length - 1)
            {
                switch(str[i + 1])
                {
                    case 'd':
                    case 'i':
                        shell_print_dec(va_arg(args, int));
                        break;
                    case 'u':
                        shell_print_dec(va_arg(args, unsigned int));
                        break;
                    case 'x':
                    case 'X':
                        shell_print_hex(va_arg(args, int));
                        break;
                    case 'c':
                        shell_print_char(va_arg(args, int));
                        break;
                    case 's':
                        shell_print(va_arg(args,char*));
                        break;
                    case '%':
                        shell_print_char('%');
                        break;

                    default:  // ERROR
                        break;
                }
                ++i;
            }
        }
        else
        {
            shell_print_char(str[i]);
        }
    }
    va_end(args);
}
