#include <commands.h>
#include <stdlib.h>
#include <syscalls.h>z

int help(string args);
int lineal(string args);
int quad(string args);
int time(string args);
int stime(string args);

struct { string name; int (*cmd)(string); } commands[] = { { "help", help }, { "lineal", lineal }, { "quad", quad }, { "time", time }, { "stime", stime }, { "", 0 } };

int command_parser(string cmd, string args)
{
    for(int i = 0; commands[i].name[0] != 0; ++i)
    {
        if(strcmp(cmd, commands[i].name))
        {
            return commands[i].cmd(args);
        }
    }
    printf("Invalid command: %s\n", cmd);
    return 1;
}

void command_run(string str)
{
    char cmd[81] = { 0 };
    char args[81] = { 0 };
    int ret;
    string separator = strfind(strcpy(cmd, str), ' ');
    if(separator != 0)
    {
        *separator = 0;
        ret = command_parser(strip(cmd, cmd), strip(args, separator + 1));
    }
    else
    {
        ret = command_parser(cmd, "");
    }
    printf("Exit code %d\n", ret);
}

int help(string args)
{
    printf("Available commands:\n");
    for(int i = 0; commands[i].name[0] != 0; ++i)
    {
        printf("  - ");
        printf(commands[i].name);
        printf("\n");
    }
    printf("\n");
    return 0;
}

int lineal(string args)
{
    uint8_t draw[25][80];
    uint64_t m = 0, b = 0;
    uint64_t sign_m, sign_b;
    int i;

    for(i = 0; args[i] != '-' && args[i] != '+'; ++i);
    if(args[i] == '-')
    {
        sign_m = -1;
    }
    else
    {
        sign_m = 1;
    }
    ++i;
    for(; args[i] >= '0' && args[i] <= '9' && args[i] != 0; ++i)
    {
        m = m * 10 + args[i] - '0';
    }
    m *= sign_m;

    for(; args[i] != '-' && args[i] != '+'; ++i);
    if(args[i] == '-')
    {
        sign_b = -1;
    }
    else
    {
        sign_b = 1;
    }
    ++i;
    for(; args[i] >= '0' && args[i] <= '9' && args[i] != 0; ++i)
    {
        b = b * 10 + args[i] - '0';
    }
    b *= sign_b;

    for(int x = 0; x < 80; ++x)
    {
        for(int y = 0; y < 25; ++y)
        {
            draw[y][x] = m * (x - 40) + b == -y + 12 ? 'X' : ' ';
        }
    }
    for(int y = 0; y < 25; ++y)
    {
        for(int x = 0; x < 80; ++x)
        {
            printf("%c", draw[y][x]);
        }
        printf("\n");
    }

    return 0;
}

int quad(string args)
{
    uint8_t draw[25][80];
    uint64_t a = 0, b = 0, c = 0;
    uint64_t sign_a, sign_b, sign_c;
    int i;

    for(i = 0; args[i] != '-' && args[i] != '+'; ++i);
    if(args[i] == '-')
    {
        sign_a = -1;
    }
    else
    {
        sign_a = 1;
    }
    ++i;
    for(; args[i] >= '0' && args[i] <= '9' && args[i] != 0; ++i)
    {
        a = a * 10 + args[i] - '0';
    }
    a *= sign_a;

    for(; args[i] != '-' && args[i] != '+'; ++i);
    if(args[i] == '-')
    {
        sign_b = -1;
    }
    else
    {
        sign_b = 1;
    }
    ++i;
    for(; args[i] >= '0' && args[i] <= '9' && args[i] != 0; ++i)
    {
        b = b * 10 + args[i] - '0';
    }
    b *= sign_b;

    for(; args[i] != '-' && args[i] != '+'; ++i);
    if(args[i] == '-')
    {
        sign_c = -1;
    }
    else
    {
        sign_c = 1;
    }
    ++i;
    for(; args[i] >= '0' && args[i] <= '9'; ++i)
    {
        c = c * 10 + args[i] - '0';
    }
    c *= sign_c;

    for(int x = 0; x < 80; ++x)
    {
        for(int y = 0; y < 25; ++y)
        {
            draw[y][x] = a * (x - 40) * (x - 40) + b * (x - 40) + c == -y + 12 ? 'X' : ' ';
        }
    }
    for(int y = 0; y < 25; ++y)
    {
        for(int x = 0; x < 80; ++x)
        {
            printf("%c", draw[y][x]);
        }
        printf("\n");
    }

    return 0;
}

int time(string args)
{
    int hours, minutes, seconds;
    get_time(&hours, &minutes, &seconds);
    if(hours < 10)
    {
        printf("0");
    }
    printf("%d:", hours);
    if(minutes < 10)
    {
        printf("0");
    }
    printf("%d:", minutes);
    if(seconds < 10)
    {
        printf("0");
    }
    printf("%d\n", seconds);

    return 0;
}

int stime(string args)
{
    int hours = 0, minutes = 0, seconds = 0;
    int i;
    for(i = 0; args[i] < '0' || args[i] > '9'; ++i);
    for(; args[i] >= '0' && args[i] <= '9' && args[i] != 0; ++i)
    {
        hours = hours * 10 + args[i] - '0';
    }
    for(; args[i] < '0' || args[i] > '9'; ++i);
    for(; args[i] >= '0' && args[i] <= '9' && args[i] != 0; ++i)
    {
        minutes = minutes * 10 + args[i] - '0';
    }
    for(; args[i] < '0' || args[i] > '9'; ++i);
    for(; args[i] >= '0' && args[i] <= '9' && args[i] != 0; ++i)
    {
        seconds = seconds * 10 + args[i] - '0';
    }
    set_time(hours, minutes, seconds);

    return 0;
}
