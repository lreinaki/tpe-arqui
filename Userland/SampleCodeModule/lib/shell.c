#include <shell.h>
#include <syscalls.h>
#include <string.h>

static char buffer[64] = { '0', 0 };
static char line[80];
static int index = 0;

static uint32_t uint2base(uint64_t value, string buffer, uint32_t base)
{
    string p = buffer;
    string p1, p2;
    uint32_t digits = 0;

    //Calculate characters for each digit
    do
    {
        uint32_t remainder = value % base;
        *p++ = (remainder < 10) ? remainder + '0' : remainder + 'A' - 10;
        ++digits;
    }
    while(value /= base);

    // Terminate string in buffer.
    *p = 0;

    //Reverse string in buffer.
    p1 = buffer;
    p2 = p - 1;
    while(p1 < p2)
    {
        char tmp = *p1;
        *p1 = *p2;
        *p2 = tmp;
        ++p1;
        --p2;
    }

    return digits;
}

void shell_print_base(uint64_t value, uint32_t base)
{
    uint2base(value, buffer, base);
    shell_print(buffer);
}

void shell_print(const string str)
{
    for(int i = 0; str[i] != 0; ++i)
    {
        shell_print_char(str[i]);
    }
}

void shell_println(const string str)
{
    shell_print(str);
    shell_newline();
}

void shell_print_char(char character)
{
    switch(character)
    {
        case '\b':
            shell_backspace();
            return;

        case '\n':
            shell_newline();
            return;

        default:
            if(index < 80)
            {
                line[index++] = character;
                write(character);
            }
            return;
    }
}

void shell_newline(void)
{
    write('\n');
    for(int i = 0; i < 80; ++i)
    {
        line[i] = 0;
    }
    index = 0;
}

void shell_backspace(void)
{
    if(index != 0)
    {
        write('\b');
        line[index--] = 0;
    }
}

void shell_print_dec(uint64_t value)
{
    shell_print_base(value, 10);
}

void shell_print_hex(uint64_t value)
{
    shell_print_base(value, 16);
}

void shell_print_bin(uint64_t value)
{
    shell_print_base(value, 2);
}

void shell_clear(void)
{
    for(int i = 0; i < 25; ++i)
    {
        shell_print_char('\n');
    }
    //clear();
}

void shell_print_time(int hours, int minutes, int seconds)
{
    if(hours < 0 || hours >= 24 || minutes < 0 || minutes >= 60 || seconds < 0 || seconds >= 60)
    {
        return;
    }
    if(hours < 10)
    {
        shell_print_char('0');
    }
    shell_print_dec(hours);
    shell_print_char(':');
    if(minutes < 10)
    {
        shell_print_char('0');
    }
    shell_print_dec(minutes);
    shell_print_char(':');
    if(seconds < 10)
    {
        shell_print_char('0');
    }
    shell_print_dec(seconds);
}
