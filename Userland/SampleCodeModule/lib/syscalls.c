#include <syscalls.h>
#include <stdint.h>
#include <int80.h>

void write(char c)
{
    int_80(0, (uint64_t)c, 0, 0);
}

/*void clear(void)
{
    int_80(1, 0, 0, 0);
}*/

void read(uint8_t* c)
{
    int_80(2, c, 0, 0);
}

void get_time(int* hours, int* minutes, int* seconds)
{
    int_80(3, (uint64_t)hours, (uint64_t)minutes, (uint64_t)seconds);
}

void set_time(int hours, int minutes, int seconds)
{
    int_80(4, (uint64_t)hours, (uint64_t)minutes, (uint64_t)seconds);
}
